使用 JekyllUtils 快速部署 Jekyll 网站到腾讯云对象存储。

# 准备

安装 [JRE8 (x64 Installer)](https://www.oracle.com/java/technologies/downloads/#jre8-windows)

> 如果不想注册 Oracle 账号可以安装 [OpenJDK](https://jdk.java.net/java-se-ri/8-MR3)



配置：

[点击获取](https://console.cloud.tencent.com/cam/capi) ： `appId` , `secretId` , `secretKey`

[点击获取](https://cloud.tencent.com/document/product/436/6224) ：`region`

```yaml
tencent.cos:
  appId: 12345678
  secretId: secretId
  secretKey: secretKey
  region: ap-guangzhou
  bucketNamePrefix: jekyll-deploy-test

jekyll:
  rootPath: E:\jekyll\turorial
```



# 使用

创建存储桶并开启静态网站：

```
java -jar jekyll-utils-0.0.1.jar bucket init
```

部署 Jekyll：

```
java -jar jekyll-utils-0.0.1.jar jekyll deploy
```

当想干掉网站时，可以 <span style="color: red">删除存储桶</span>：

```
java -jar jekyll-utils-0.0.1.jar bucket delete
```

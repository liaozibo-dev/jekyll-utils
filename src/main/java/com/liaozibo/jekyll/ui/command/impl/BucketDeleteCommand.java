package com.liaozibo.jekyll.ui.command.impl;

import com.liaozibo.jekyll.service.CosService;
import com.liaozibo.jekyll.ui.command.BaseCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BucketDeleteCommand extends BaseCommand {
    @Autowired
    private CosService cosService;

    @Override
    public String getSubject() {
        return "bucket";
    }

    @Override
    public String getAction() {
        return "delete";
    }

    @Override
    public void execute(String[] args) {
        cosService.deleteBucket();
    }
}

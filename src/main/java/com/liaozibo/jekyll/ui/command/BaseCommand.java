package com.liaozibo.jekyll.ui.command;

public abstract class BaseCommand {
    private static final String SEPARATOR = ":";

    abstract public String getSubject();
    abstract public String getAction();
    abstract public void execute(String[] args);

    public String getKey() {
        return BaseCommand.getKey(getSubject(), getAction());
    }

    public static String getKey(String subject, String action) {
        return subject + SEPARATOR + action;
    }
}

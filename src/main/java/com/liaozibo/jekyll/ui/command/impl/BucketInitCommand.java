package com.liaozibo.jekyll.ui.command.impl;

import com.liaozibo.jekyll.service.CosService;
import com.liaozibo.jekyll.ui.command.BaseCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BucketInitCommand extends BaseCommand {

    @Autowired
    private CosService cosService;

    @Override
    public String getSubject() {
        return "bucket";
    }

    @Override
    public String getAction() {
        return "init";
    }

    @Override
    public void execute(String[] args) {
        cosService.initBucket();
    }

}

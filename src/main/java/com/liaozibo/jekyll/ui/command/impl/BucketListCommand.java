package com.liaozibo.jekyll.ui.command.impl;

import com.liaozibo.jekyll.service.CosService;
import com.liaozibo.jekyll.ui.command.BaseCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BucketListCommand extends BaseCommand {
    @Autowired
    private  CosService cosService;

    @Override
    public String getSubject() {
        return "bucket";
    }

    @Override
    public String getAction() {
        return "list";
    }

    @Override
    public void execute(String[] args) {
        cosService.listBuckets();
    }
}

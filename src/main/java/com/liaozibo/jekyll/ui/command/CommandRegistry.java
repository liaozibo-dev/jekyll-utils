package com.liaozibo.jekyll.ui.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class CommandRegistry {
    private Map<String, BaseCommand> register = new HashMap<>();

    @Autowired
    private ApplicationContext context;

    @PostConstruct
    public void init() {
        Map<String, BaseCommand> beans = context.getBeansOfType(BaseCommand.class);
        for (BaseCommand command : beans.values()) {
            register.put(command.getKey(), command);
        }
    }

    public BaseCommand getCommand(String key) {
        return register.get(key);
    }

    public Collection<BaseCommand> getCommandList() {
        return register.values();
    }
}

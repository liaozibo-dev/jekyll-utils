package com.liaozibo.jekyll.ui.command.impl;

import com.liaozibo.jekyll.service.CosService;
import com.liaozibo.jekyll.ui.command.BaseCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JekyllDeployCommand extends BaseCommand {
    @Autowired
    private CosService cosService;

    @Override
    public String getSubject() {
        return "jekyll";
    }

    @Override
    public String getAction() {
        return "deploy";
    }

    @Override
    public void execute(String[] args) {
        try {
            cosService.deploy();
        } catch (InterruptedException e) {
            log.error("部署失败：" + e.getMessage());
        }
    }
}

package com.liaozibo.jekyll.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@ConfigurationProperties("jekyll")
@Component
@Data
public class JekyllProperties {
    private static final String SITE_PATH = "_site";

    private String rootPath;
    public Path getSitePath() {
        return Paths.get(rootPath, SITE_PATH);
    }
}

package com.liaozibo.jekyll.property;

import com.liaozibo.jekyll.constant.CosRegionConsts;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("tencent.cos")
@Component
@Data
public class TencentCosProperties {
    private String appId;
    private String secretKey;
    private String secretId;
    private String region = CosRegionConsts.SHANG_HAI;
    private String bucketNamePrefix;

    /**
     * 存储桶名称，格式：BucketName-APPID
     * */
    public String getBucketName() {
        return bucketNamePrefix + "-" + appId;
    }

    /**
     * 静态网站 URL
     * */
    public String getStaticSiteUrl() {
        String url = String.format("https://%s.cos-website.%s.myqcloud.com", getBucketName(), getRegion());
        return url;
    }
}

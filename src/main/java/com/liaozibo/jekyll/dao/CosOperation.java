package com.liaozibo.jekyll.dao;

import java.nio.file.Path;
import java.util.List;

public interface CosOperation {
    /**
     * @return 存储桶名称列表
     * */
    List<String> listBucketNames();

    /**
     * 创建存储桶
     * */
    void createBucket(String bucketName);

    /**
     * 启用静态网站功能
     * */
    void enableStaticSite(String bucketName);

    /**
     * 删除存储桶
     * */
    void deleteBucket(String bucketName);

    /**
     * 清空存储桶
     * */
    void truncate(String bucketName);

    /**
     * 完整上传目录下的文件到存储的根目录
     * */
    void uploadDirectory(Path localDir, String bucketName) throws InterruptedException;
}

package com.liaozibo.jekyll.constant;

/**
 * https://cloud.tencent.com/document/product/436/6224
 */
public class CosRegionConsts {
    public static final String BEI_JING = "ap-beijing";
    public static final String NAN_JING = "ap-nanjing";
    public static final String SHANG_HAI = "ap-shanghai";
    public static final String GUANG_ZHOU = "ap-guangzhou";
    public static final String CHENG_DU = "ap-chengdu";
    public static final String CHONG_QING = "ap-chongqing";
}

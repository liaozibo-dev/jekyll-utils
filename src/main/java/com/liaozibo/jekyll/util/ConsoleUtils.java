package com.liaozibo.jekyll.util;

public class ConsoleUtils {
    private ConsoleUtils() {}

    public static final void stdout(String prompt) {
        System.out.println(prompt);
    }
}

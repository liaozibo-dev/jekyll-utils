package com.liaozibo.jekyll;

import com.liaozibo.jekyll.ui.ConsoleUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class JekyllUtilsApplication {
	private static String[] args = new String[0];

	@Autowired
	private ConsoleUI consoleUI;

	public static void main(String[] args) {
		JekyllUtilsApplication.args = args;
		SpringApplication.run(JekyllUtilsApplication.class, args);
	}

	@PostConstruct
	public void run() {
		consoleUI.run(args);
	}
}
